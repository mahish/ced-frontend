import { _toggleState } from './utilities';

export default class Modal {
  constructor($modal, options = {}) {
    this.$root = document.documentElement;
    this.$modal = $modal;
    this.modal = this.$modal.id;
    this.$toggles = document.querySelectorAll(`[data-toggle="modal"][data-target="${this.modal}"]`);
    this.$backdrop = $modal.querySelector('[data-modal-backdrop]');
    this.$menu = options.$menu || document.querySelector('#navigation');

    if (!this.$toggles) return console.warn('No toggle found!', `#${this.modal}`);

    this.isOpen = false;
    this.navigation = {
      $active: this.$menu.querySelector('.navigation a.active'),
      $toggle: this.$menu.querySelector(`.navigation [data-toggle="modal"][data-target="${this.modal}"]`),
    };

    this.dismiss = this.dismiss.bind(this);

    this.addEventListeners();
  }

  addEventListeners() {
    this.$backdrop.addEventListener('click', (event) => {
      this.toggle();
      event.preventDefault();
      event.stopPropagation();
    }, false);

    this.$toggles.forEach(($toggle) => {

      $toggle.addEventListener('click', (event) => {
        this.toggle();
        event.preventDefault();
        event.stopPropagation();
      });
    });
  }

  toggle() {
    if (this.isOpen) {
      this.hide();
    } else {
      this.open();
    }

    if (this.navigation.$toggle) {
      this.$root.classList.toggle('is-modal--information');
      this.navigation.$active.classList.toggle('active');
      this.navigation.$toggle.classList.toggle('active');
      this.navigation.$toggle.blur();
      this.$menu.setAttribute('data-state', 'closed');
    }
  }

  dismiss() {
    this.$modal.style.display = '';
    this.$modal.removeEventListener('transitionend', this.dismiss, true);
  }

  hide() {
    this.$modal.addEventListener('transitionend', this.dismiss, true);

    this.$root.classList.remove('modal-open');
    this.$modal.classList.remove('show');
    this.$modal.style.pointerEvents = 'none';

    this.isOpen = false;
  }

  open() {
    this.$modal.style.display = 'block';

    setTimeout(() => {
      this.$root.classList.toggle('modal-open');
      this.$modal.classList.toggle('show');
      // this.$modal.style.opacity = '1';
      this.$modal.style.pointerEvents = '';
    }, 30);

    this.isOpen = true;
  }
}
