// ==========================================
// TABLE OF CONTENT
// ==========================================
// 1. Dependencies
// 2. Config
// 3. Functions
// 4. Gulp tasks


// ==========================================
// 1. DEPENDENCIES
// ==========================================
// gulp-dev-dependencies
const {
  dest, lastRun, parallel, series, src, watch
} = require('gulp');
// check package.json for gulp plugins
const gulpLoadPlugins = require('gulp-load-plugins');

// dev-dependencies
const browserSync = require('browser-sync').create();
const del = require('del');
const fs = require('fs');
const { join } = require('path');
const { rollup } = require('rollup');
const { babel } = require('@rollup/plugin-babel');
const commonjs = require('@rollup/plugin-commonjs');
const { nodeResolve } = require('@rollup/plugin-node-resolve');
const { terser } = require('rollup-plugin-terser');

const postcssAutoprefixer = require('autoprefixer');
const postcssCssnano = require('cssnano');
const postcssNormalize = require('postcss-normalize');

const imageSize = require('image-size');
const slugify = require('slugify');

const $ = gulpLoadPlugins();

// ==========================================
// 2. CONFIG
// ==========================================
const config = {
  // BUILD TASKS
  tasks: ['pug', 'css', 'js', 'images:ui', 'images:projects', 'fonts'],
  // COMMAND ARGUMENTS
  cmd: {
    // check if "gulp --production"
    // http://stackoverflow.com/questions/28538918/pass-parameter-to-gulp-task#answer-32937333
    production: process.argv.indexOf('--production') > -1 || false,
  },
  // FOLDERS
  src: {
    folder: 'src/',
    data: {
      folder: 'src/data/',
      files: 'src/data/**/*.json',
    },
    fonts: 'src/fonts/**/*.*',
    public: 'src/public/**/*.*',
    img: {
      files: 'src/img/**/*.{jpg,png,svg,gif}',
      root: 'src/img/*.{jpg,png,gif}',
      favicon: 'src/img/favicon/**/*',
    },
    videos: {
      files: 'src/videos/**/*.mp4',
      projects: 'src/img/projects/**/*.mp4',
    },
    js: {
      folder: 'src/js/',
      files: 'src/js/**/*.js',
      main: 'src/js/main.js',
      animation: 'src/js/animation.js',
      pdf: 'src/js/pdf.js',
      library: 'src/js/lib/',
      vendor: [
        'src/js/vendor/**/*.js',
      ],
      vendorFiles: 'src/js/vendor/**/*.js',
    },
    pug: {
      files: 'src/pug/**/*.pug',
      pages: 'src/pug/pages/**/*.pug',
      // projects: 'src/pug/projects/**/*.pug',
      partials: 'src/pug/_partials/**/*.pug',
    },
    scss: 'src/scss/**/*.scss',
  },
  tmp: {
    folder: 'tmp/',
    data: {
      folder: 'tmp/data/',
      file: 'tmp/data/data.json',
    },
    img: {
      folder: 'tmp/img/',
      files: 'tmp/img/**/*.{jpg,png,svg,gif}',
      projects: 'tmp/img/projects/**/*.{jpg,png}',
    },
  },
  dist: {
    folder: 'dist/',
    css: 'dist/assets/css/',
    fonts: 'dist/assets/fonts/',
    public: 'dist/assets/public/',
    img: 'dist/assets/img/',
    videos: 'dist/assets/videos/',
    js: 'dist/assets/js/',
    jsVendor: 'dist/assets/js/vendor/',
  },
  // plugin settings
  // AUTORPEFIXER
  autoprefixer: {
    cascade: true,
    precision: 10,
  },
  // SERVER
  browserSync: {
    // proxy: 'localhost:8888/display/web/',
    server: {
      baseDir: 'dist/',
      // directory: true,
    },
    files: '**/*.html',
    ghostMode: {
      click: true,
      // location: true,
      forms: true,
      scroll: true,
    },
    injectChanges: true,
    logFileChanges: true,
    logLevel: 'info',
    notify: false,
    reloadDelay: 100,
    // startPath: "/cviceni/"
  },
  // IMAGES
  images: {
    sizes: [
      750,
      980,
      1490,
      2000,
    ],
  },
  // POSTCSS
  postcss: {
    plugins: [
      postcssNormalize(),
      postcssAutoprefixer(),
      postcssCssnano(),
    ],
  },
  // PUG
  pug: {
    locals: {
      // baseUrl: process.argv.indexOf('--production') > -1 ? '//jirimaha.com/michal-landa/new/' : '/',
      baseUrl: '/',
      imageSize,
      slugify,
      // fileSize,
      // flatten,
    },
  },
  // ROLLUP
  rollup: {
    main: {
      bundle: {
        input: 'src/js/main.js',
        plugins: [
          nodeResolve(),
          commonjs(),
          babel({ babelHelpers: 'bundled' }),
          terser(),
        ],
      },
      output: {
        file: 'dist/assets/js/main.build.js',
        format: 'iife',
        name: 'ced',
        sourcemap: true,
      },
    },
    animation: {
      bundle: {
        input: 'src/js/animation.js',
        plugins: [
          nodeResolve(),
          commonjs(),
          babel({ babelHelpers: 'bundled' }),
          terser(),
        ],
      },
      output: {
        file: 'dist/assets/js/animation.build.js',
        format: 'iife',
        name: 'cedAnimation',
        sourcemap: true,
      },
    },
    pdf: {
      bundle: {
        input: 'src/js/pdf.js',
        plugins: [
          nodeResolve(),
          commonjs(),
          babel({ babelHelpers: 'bundled' }),
          terser(),
        ],
      },
      output: {
        file: 'dist/assets/js/pdf.build.js',
        format: 'iife',
        name: 'cedPDF',
        sourcemap: true,
      },
    },
  },
  // SASS
  sass: {
    errLogToConsole: true,
    outputStyle: 'compressed',
    includePaths: [
      join(__dirname, 'node_modules') // npm
    ],
  },
};


// ==========================================
// 3. FUNCTIONS
// ==========================================
function serve(done) {
  if (browserSync.active) {
    return;
  }
  browserSync.init(config.browserSync);
  done();
}

function reload(done) {
  browserSync.reload();
  done();
}


// ==========================================
// 4. TASKS
// ==========================================
// CLEAN
function clean() {
  return del([config.dist.folder, config.tmp.folder]);
}


// DATA
function dataMerge() {
  return src(config.src.data.files)
    .pipe($.plumber(config.plumber))
    // .pipe($.debug({
    //   title: 'data-source:',
    // }))
    .pipe($.jsoncombine('data.json', data => Buffer.from(JSON.stringify(data))))
    // .pipe($.debug({ title: 'data-result:' }))
    .pipe(dest(config.tmp.data.folder));
}


// PUG
function pug() {
  return src(config.src.pug.pages)
    .pipe($.plumber({ errorHandler: (err) => {
      console.log(err);
      return err;
    }}))
    // .pipe($.debug({
    //   title: 'pug:',
    // }))
    .pipe($.data(() => JSON.parse(fs.readFileSync(config.tmp.data.file))))
    .pipe($.pug(config.pug))
    // .pipe($.debug({
    //   title: 'html:',
    // }))
    .pipe($.rename((path) => {
      // Returns a completely new object, make sure you return all keys needed!
      const obj = {
        dirname: '/',
        basename: 'index',
        extname: '.html',
      };

      // if root
      if (path.dirname === '.') {

        // if homepage
        if (path.basename === 'index') {
          return obj;
        }

        obj.dirname = path.basename;

        return obj;
      }
      // if nested page
      else {
        obj.dirname = join(path.dirname, path.basename);

        return obj;
      }
    }))
    .pipe(dest(config.dist.folder));
}


// SASS
function css() {
  return src(config.src.scss, { sourcemaps: true })
    .pipe($.sass(config.sass).on('error', $.sass.logError))
    .pipe($.postcss(config.postcss.plugins))
    .pipe(dest(config.dist.css, { sourcemaps: './maps' }))
    .pipe(browserSync.stream({ match: '**/*.css' }));
}


// JAVASCRIPT
// JS:VENDOR
function jsVendor() {
  return src(config.src.js.vendor)
    .pipe(dest(config.dist.js));
}

// main
async function jsMain() {
  const bundle = await rollup(config.rollup.main.bundle);
  bundle.write(config.rollup.main.output);
}
async function jsAnimation() {
  const bundle = await rollup(config.rollup.animation.bundle);
  bundle.write(config.rollup.animation.output);
}
async function jsPDF() {
  const bundle = await rollup(config.rollup.pdf.bundle);
  bundle.write(config.rollup.pdf.output);
}


// IMAGES: Resize all project images
function imagesToResize() {
  return src(config.src.img.root, {
    // since: lastRun(imagesToResize),
  })
    .pipe(dest(config.dist.img))

    // .pipe($.changed(`${config.dist.img}/projects/`))
    // .pipe(parallel(
    //   // imageResize({ width : 100 }),
    //   $.imageResize({
    //     width: size,
    //     quality: 75,
    //   }),
    //   os.cpus().length,
    // ))
    .pipe($.responsive({
      '*.{png,jpg}': [
        {
          blur: 15,
          // median: 20,
          width: 15,
          // format: 'jpg',
          quality: 77,
          // lossless: true,
          // nearLossless: 80,
          rename: {
            suffix: '-placeholder',
          },
        }
      ],
      '*.jpg': [
        {
          width: 750,
          // format: 'jpg',
          quality: 85,
          // lossless: true,
          // nearLossless: 80,
          rename: {
            suffix: '-750w',
          },
        },
        {
          width: 1490,
          // format: 'jpg',
          quality: 85,
          // lossless: true,
          // nearLossless: true,
          rename: {
            suffix: '-1490w',
          },
        },
      ],
    },
    {
      errorOnUnusedImage: false, // !config.cmd.production,
      errorOnEnlargement: false, // !config.cmd.production,
      silent: true, // config.cmd.production,
    }))
    .pipe($.if(config.cmd.production, $.imagemin()))
    .pipe(dest(config.dist.img));
}

// IMAGES: Process all non-project images
function imagesUi() {
  return src([config.src.img.files, `!${config.src.img.root}`, `!${config.src.img.favicon}`])
    .pipe($.if(config.cmd.production, $.imagemin()))
    .pipe(dest(config.dist.img));
}


// IMAGES: Favicon
function imageFavicon() {
  return src(config.src.img.favicon)
    .pipe(dest(`${config.dist.img}/favicon/`));
}

// VIDEOS
// function videos() {
//   return src(config.src.videos.projects)
//     .pipe(dest(`${config.dist.videos}/projects/`));
// }

// FONTS
function fonts() {
  return src(config.src.fonts)
    .pipe(dest(config.dist.fonts));
}
// PUBLIC
function public() {
  return src(config.src.public)
    .pipe(dest(config.dist.public));
}

// WATCH
function watcher(done) {
  // data
  watch(config.src.data.files, series(dataMerge, pug, reload));
  // pug
  watch(config.src.pug.files, series(pug, reload));
  // css
  watch(config.src.scss, series(css));
  // js:app
  watch([config.src.js.files, `!${config.src.js.pdf}`, `!${config.src.js.vendorFiles}`], series(jsMain, reload));
  watch([config.src.js.animation], series(jsAnimation, reload));
  watch([config.src.js.pdf], series(jsPDF, reload));
  // js:vendor
  watch(config.src.js.vendorFiles, series(jsVendor, reload));
  // images
  watch([config.src.img.files, `!${config.src.img.projects}`], series(imagesUi, reload));
  watch(config.src.img.root, series(imagesToResize, reload));
  // videos
  // watch(config.src.videos.projects, series(videos, reload));
  // fonts
  watch(config.src.fonts, series(fonts, reload));

  done();
}

// GULP
exports.default = series(
  clean,
  parallel(
    series(
      dataMerge,
      pug,
    ),
    css,
    jsMain,
    jsAnimation,
    jsPDF,
    jsVendor,
    // videos,
    imagesToResize,
    imagesUi,
    // imageFavicon,
    fonts,
    public,
  ),
  serve,
  watcher,
);

exports.build = series(
  clean,
  parallel(
    series(
      dataMerge,
      pug,
    ),
    css,
    jsMain,
    jsAnimation,
    jsPDF,
    jsVendor,
    // videos,
    imagesToResize,
    imagesUi,
    // imageFavicon,
    fonts,
    public,
  ),
);
