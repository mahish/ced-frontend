const domLoad = () => {
  // If absolute URL from the remote server is provided, configure the CORS
  // header on that server.
  // const url = "https://assets.codepen.io/58289/text+-+ruin+-+ENG+-+FIN.+c-e+AO+10.03.21+-+JM+IM+JM+2.pdf";

  // Loaded via <script> tag, create shortcut to access PDF.js exports.
  const pdfjsLib = window["pdfjs-dist/build/pdf"];

  // The workerSrc property shall be specified.
  pdfjsLib.GlobalWorkerOptions.workerSrc =
    "https://cdn.jsdelivr.net/npm/pdfjs-dist@2.7.570/build/pdf.worker.min.js"; // replace with local version

  const $pdfOpens = document.querySelectorAll(
    "[data-action='click->pdf#open']"
  );
  const $pdf = document.querySelector("[data-controller='pdf']");
  const $pdfControls = $pdf.querySelector(".pdf__controls");
  const $pdfPrevious = $pdf.querySelector(
    "[data-action='click->pdf#previous']"
  );
  const $pdfNext = $pdf.querySelector("[data-action='click->pdf#next']");
  const $pdfClose = $pdf.querySelector("[data-action='click->pdf#close']");
  const $pdfCount = $pdf.querySelector("[data-pdf-target='count']");
  const $pdfNum = $pdf.querySelector("[data-pdf-target='num']");
  const $pdfDownload = $pdf.querySelector("[data-pdf-target='download']");
  const $pdfView = $pdf.querySelector("[data-pdf-target='view']");
  const $pdfSpinner = $pdf.querySelector("[data-pdf-target='spinner']");
  const ctx = $pdfView.getContext("2d");

  const scale = 0.9;

  const windowViewport = {};
  windowViewport.width = window.innerWidth * scale;
  windowViewport.height = window.innerHeight * scale;
  windowViewport.ratio = windowViewport.width / windowViewport.height;

  let pdfDoc = null;
  let pageRendering = false;
  let pageNum = 1;
  let pageNumPending = null;

  function setSize(page) {
    const pdfViewport = page.getViewport({ scale: scale });
    const getScale = (width) => scale * (width / pdfViewport.width);

    const pdf = {};
    pdf.ratio = pdfViewport.width / pdfViewport.height;

    if (pdf.ratio >= windowViewport.ratio) {
      pdf.width = windowViewport.width;
      pdf.height = windowViewport.width / pdf.ratio;
      pdf.scale = getScale(pdf.width);
    } else {
      pdf.width = windowViewport.height * pdf.ratio;
      pdf.height = windowViewport.height;
      pdf.scale = getScale(pdf.width);
    }

    return pdf;
  }

  /**
   * Get page info from document, resize canvas accordingly, and render page.
   * @param num Page number.
   */
  function renderPage(num) {
    pageRendering = true;
    // Using promise to fetch the page
    pdfDoc.getPage(num).then((page) => {
      const { width, height, scale } = setSize(page);
      const viewport = page.getViewport({ scale: scale });
      $pdfView.height = height;
      $pdfView.width = width;
      $pdfControls.style.width = `${width}px`;

      // Render PDF page into canvas context
      const renderContext = {
        canvasContext: ctx,
        viewport,
      };
      const renderTask = page.render(renderContext);

      $pdf.classList.remove("pdf--is-loading");

      // Wait for rendering to finish
      renderTask.promise.then(() => {
        pageRendering = false;
        if (pageNumPending !== null) {
          // New page rendering is pending
          renderPage(pageNumPending);
          pageNumPending = null;
        }
      });
    });

    // Update page counters
    $pdfNum.textContent = num;
  }

  /**
   * If another page rendering in progress, waits until the rendering is
   * finised. Otherwise, executes rendering immediately.
   */
  function queueRenderPage(num) {
    if (pageRendering) {
      pageNumPending = num;
    } else {
      renderPage(num);
    }
  }

  /**
   * Displays previous page.
   */
  function onPrevPage() {
    if (pageNum <= 1) {
      return;
    }
    pageNum--;
    queueRenderPage(pageNum);
  }
  $pdfPrevious.addEventListener("click", onPrevPage);

  /**
   * Displays next page.
   */
  function onNextPage() {
    if (pageNum >= pdfDoc.numPages) {
      return;
    }
    pageNum++;
    queueRenderPage(pageNum);
  }
  $pdfNext.addEventListener("click", onNextPage);

  /**
   * Asynchronously downloads PDF.
   */
  function openPDF(url) {
    pdfjsLib.getDocument(url).promise.then((pdfDoc_) => {
      pdfDoc = pdfDoc_;
      $pdfCount.textContent = pdfDoc.numPages;

      // Initial/first page rendering
      renderPage(pageNum);
    });
  }

  function prefetchPDF(url) {
    const $link = document.createElement("link");
    $link.href = url;
    $link.rel = "preload";
    $link.as = "fetch";
    $link.type = "application/pdf";
    document.head.appendChild($link);
  }

  // open PDF
  $pdfOpens.forEach(($pdfOpen) => {
    const fileUrl = $pdfOpen.dataset.pdfUrlValue;
    // TODO: prefetch first two, other on mousenter
    prefetchPDF(fileUrl);

    $pdfDownload.href = fileUrl;

    $pdfOpen.addEventListener("click", (event) => {
      const $image = $pdfOpen.querySelector('img').cloneNode();
      $pdfSpinner.appendChild($image);
      document.documentElement.classList.add("pdf-is-open");
      openPDF(fileUrl);
      event.preventDefault();
    });
  });

  // close PD
  $pdfClose.addEventListener("click", () => {
    $pdf.classList.add("pdf--is-loading");
    document.documentElement.classList.remove("pdf-is-open");
    $pdfSpinner.innerHTML = '';
    pageNum = 1;
  });
};

window.addEventListener("load", domLoad, false);
