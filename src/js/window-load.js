import Flickity from "flickity";
import lozad from "lozad";

const windowLoad = () => {

  const $lozads = document.querySelectorAll("[data-lozad]");
  if ($lozads.length > 0) {
    const lazyLoad = lozad($lozads, {
      rootMargin: "100% 100%", // syntax similar to that of CSS Margin
      // threshold: 0, // ratio of element convergence

      loaded: function ($element) {
        const $parent = $element.parentElement;

        // if carousel, load all images at once
        if ($parent.getAttribute("data-carousel-item")) {
          const $carouselItems = $parent.parentElement.querySelectorAll(
            "[data-src]"
          );
          $carouselItems.forEach(($carouselItem) =>
            lazyLoad.triggerLoad($carouselItem)
          );
        }
      },
    });
    // lazy loads elements with default selector as '.lozad'
    lazyLoad.observe();

    $lozads.forEach(($lozad) => {
      if (!$lozad.getAttribute("data-background-image")) {
        $lozad.onload = () => {
          if ($lozad.currentSrc.indexOf("-placeholder") === -1) {
            $lozad.setAttribute("data-fully-loaded", true);
          }
        };
      }
    });
  }

  const $lozadVideos = document.querySelectorAll("[data-lozad-video]");
  if ($lozadVideos.length > 0) {
    const lazyLoad = lozad($lozadVideos, {
      rootMargin: "38% 38%", // syntax similar to that of CSS Margin
      // threshold: 0, // ratio of element convergence
    });
    // lazy loads elements with default selector as '.lozad'
    lazyLoad.observe();
  }

  const $flickitys = document.querySelectorAll('[data-controller="carousel"]');
  if ($flickitys.length > 0) {
    $flickitys.forEach(($flickity) => {
      const options = JSON.parse($flickity.dataset.options || '');
      const flickity = new Flickity($flickity, {
        // arrowShape: 'M0 .5h98.79L86.6 12.69',
        autoPlay: false,
        cellAlign: "center",
        cellSelector: ".carousel__cell",
        contain: false,
        pageDots: false,
        dragThreshold: 1,
        freeScroll: true,
        // fullscreen: true,
        // groupCells: true,
        // percentPosition: true,
        prevNextButtons: false,
        // setGallerySize: false,
        setGallerySize: options.setGallerySize || false,
        // wrapAround: options.wrapAround || true,
        wrapAround: options.wrapAround !== undefined ? options.wrapAround : true,
        // imagesLoaded: true,
      });

      // const $cellElements = flickity.getCellElements();
      // $cellElements.forEach($cellElement => {
      //   $cellElement.addEventListener('click', () => {
      //     flickity.viewFullscreen();
      //   });
      // });

      // flickity.on('dragStart', (event, pointer) => {
      //   APP.flickity.drag = true;
      // });
      // flickity.on('dragEnd', (event, pointer) => {
      //   setTimeout(() => {
      //     APP.flickity.drag = false;
      //   }, 50);
      // });

      const $currentIndex = $flickity.querySelector('[data-carousel-target="currentIndex"]');
      const $caption = $flickity.querySelector(
        '[data-carousel-target="caption"]'
      );

      flickity.on("change", (event) => {

        if ($currentIndex) {
          $currentIndex.textContent = event + 1;
        }

        if ($caption) {
          $caption.innerHTML =
            flickity.selectedElement.dataset.carouselCaptionValue;
        }
      });

      const $navButtons = document.querySelectorAll(
        '[data-action^="click>carousel#"]'
      );
      if ($navButtons.length > 0) {
        $navButtons.forEach(($navButton) => {
          $navButton.addEventListener("click", () => {
            switch ($navButton.dataset.action) {
              case "click>carousel#previous":
                flickity.previous();
                break;

              case "click>carousel#next":
                flickity.next();
                break;

              default:
                break;
            }
          });
        });
      }
    });
  }

  document.querySelector(".loading").classList.remove("loading");
};

export default windowLoad;
