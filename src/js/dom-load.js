// import Flickity from 'flickity';
// import lozad from 'lozad';
// import Zooming from 'zooming';
import _throttle from 'lodash.throttle';
// import Lightbox from './lightbox';
import Modal from './modal';
// import animation from './animation';
import { _toggleState } from './utilities';

const domLoad = () => {
  const mobileMediaQuery = window.matchMedia('(max-width: 1024px)');
  //mobileMediaQuery.addListener(this.onMobileBreakpointChange);

  const $body = document.body;

  const $header = document.querySelector('#header');
  if ($header) {
    // Toggle Header Depending on Scrolling Direction
    // https://codepen.io/tutsplus/pen/WNerWWp
    const cssScroll = {
      down: 'scroll--down',
      up: 'scroll--up',
      zero: 'scroll--zero',
    };
    let lastScroll = 0;

    window.addEventListener('scroll', _throttle(() => {
      const currentScroll = window.pageYOffset;

      if (currentScroll <= 0) {
        $body.classList.remove(cssScroll.down);
        $body.classList.remove(cssScroll.up);
        $body.classList.add(cssScroll.zero);
        return;
      } else {
        $body.classList.remove(cssScroll.zero);
      }

      if (currentScroll <= window.innerHeight * .062) {
        // safety zone
        $body.classList.remove(cssScroll.down);
      }
      else if (currentScroll > lastScroll && !$body.classList.contains(cssScroll.down)) {
        // down
        $body.classList.add(cssScroll.down);
        $body.classList.remove(cssScroll.up);
      }
      else if (currentScroll < lastScroll && $body.classList.contains(cssScroll.down)) {
        // up
        $body.classList.add(cssScroll.up);
        $body.classList.remove(cssScroll.down);
      }

      lastScroll = currentScroll;

    }, 100));
  }


  if (mobileMediaQuery.matches) {
    // Mobile menu
    const $menu = $header.querySelector('#navigation');
    const $mobileMenuToggle = document.querySelector('#mobile-menu-toggle');
    $mobileMenuToggle.addEventListener(
      'click',
      (event) => {
        _toggleState($menu, 'closed', 'open');
        _toggleState($mobileMenuToggle, 'closed', 'open');
        $body.classList.toggle('is-menu');
        event.preventDefault();
      },
      false
    );

    const $navigationSecondary = $header.querySelector('[data-controller="navigation-secondary"]');
    if ($navigationSecondary) {
      const $active = $navigationSecondary.querySelector('.active');

      $active.addEventListener('click', (event) => {
        event.preventDefault();
        event.stopPropagation();
        _toggleState($navigationSecondary, 'closed', 'open');
      }, false)
    }
  }

  // video need different lazy load options
  const $lozadVideos = document.querySelectorAll('[data-lozad-video]');
  if ($lozadVideos.length > 0) {
    $lozadVideos.forEach(($lozadVideo) => {

      $lozadVideo.addEventListener('loadeddata', () => {
        $lozadVideo.setAttribute('data-fully-loaded', true);
      });

      if ($lozadVideo.hasAttribute('data-autoplay')) {
        $lozadVideo.addEventListener('canplay', () => $lozadVideo.play());
      }

      const $playButton = $lozadVideo.parentElement.querySelector('[data-action="play"]');
      if ($playButton) {
        $lozadVideo.addEventListener('ended', () => {
          $playButton.classList.remove('is-hidden');
        });
        $lozadVideo.addEventListener('playing', () => {
          $playButton.classList.add('is-hidden');
        });

        $playButton.addEventListener('click', () => {
          $lozadVideo.play();
        });
      }
    });
  }

  // const $lightboxToggles = document.querySelectorAll('[data-lightbox__toggle]');
  // if ($lightboxToggles.length > 0) {
  //   $lightboxToggles.forEach(($lightboxToggle) => {
  //     const lightboxToggle = new Lightbox($lightboxToggle);
  //   });
  // }

  // // Lightbox (zooming)
  // const zooming = new Zooming({
  //   // bgOpacity: 0,
  //   // customSize: '100%',
  //   enableGrab: false,
  //   // preloadImage: true,
  //   scaleBase: 0.8,
  //   scrollThreshold: 20,
  //   // transitionDuration: 0.0001,
  //   zIndex: 1000,
  // });
  // zooming.listen('.zooming');

  // Modal
  const $modals = document.querySelectorAll('[data-modal]');
  if ($modals.length > 0) {
    $modals.forEach(($modal) => {
      const modal = new Modal($modal, { $menu });
    });
  }

  const $scatteredTexts = document.querySelectorAll('[data-controller="scattered-text"]');
  if ($scatteredTexts.length > 0) {

    // eslint-disable-next-line no-inner-declarations
    function getRandomInt(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
    }

    $scatteredTexts.forEach(($scatteredText) => {
      let newText = '';
      const lines = $scatteredText.innerHTML.split('<br>');

      lines.forEach(line => {
        const chars = line.split('');

        chars.forEach(char => {
          const sign = getRandomInt(1, 100) % 2 ? '-' : '';
          const rotation = getRandomInt(0, 30);
          const y = getRandomInt(0, 10);
          const isArrow = char === '→';
          const isSpace = char === ' ';
          const styles = isArrow || isSpace ? false : `transform: translateY(${sign}${y}%) rotate(${sign}${rotation}deg);`;

          newText += `<span style="${styles}">${isSpace ? '&thinsp;&thinsp;' : char}</span>`;
        });

        newText += '<br/>';
      });



      $scatteredText.innerHTML = newText;
    });
  }

  // const $carousels = document.querySelectorAll('[data-carousel="default"]');
  // if ($carousels.length > 0 || $lightboxToggles.length > 0) {
  //   idleTimer({
  //     // function to fire after idle
  //     callback: () => document.documentElement.classList.add('is-idle'),
  //     // function to fire when active
  //     activeCallback: () => document.documentElement.classList.remove('is-idle'),
  //     // Amount of time in milliseconds before becoming idle. default 60000
  //     idleTime: 2000,
  //   });
  // }

  const $theaters = document.querySelectorAll('[data-controller="theater"]');
  if ($theaters.length > 0) {
    const $theaterTooltip = document.querySelector('[data-theater-target="tooltip"]');
    const $theaterIndicator = $theaterTooltip.querySelector('[data-theater-target="indicator"]');
    const $theaterUrl = $theaterTooltip.querySelector('[data-theater-target="url"]');
    const $theaterName = $theaterTooltip.querySelector('[data-theater-target="name"]');
    let timerFn;

    $theaters.forEach($theater => {
      const theaterBCR = $theater.getBoundingClientRect();
      $theaterTooltip.style.top = `${theaterBCR.bottom}px`;

      if (mobileMediaQuery.matches) {
        $theater.addEventListener('click', (event) => event.preventDefault());
      }

      $theater.addEventListener('mouseenter', () => {
        $theaterTooltip.classList.add('is-active');
        $theaterUrl.setAttribute('href', $theater.href);
        $theaterName.innerHTML = $theater.dataset.theaterNameValue;
        const tooltipLeft = $theaterTooltip.getBoundingClientRect().left;
        $theaterIndicator.style.left = `${(theaterBCR.left + theaterBCR.width / 2 - tooltipLeft).toFixed(2)}px`;

        clearTimeout(timerFn);
      });

      $theater.addEventListener('mouseleave', () => {
        timerFn = setTimeout(() => {
          $theaterTooltip.classList.remove('is-active');
        }, 500);
      });

    });
  }
};

export default domLoad;
