import Glide from '@glidejs/glide';

export default class Lightbox {
  constructor($toggle) {
    this.$root = document.body;
    this.$toggle = $toggle;
    this.lightbox = this.$toggle.getAttribute('data-lightbox__toggle');
    this.$lightbox = document.getElementById(this.lightbox);
    // this.$backdrop = this.$lightbox.querySelector('.lightbox__backdrop');
    this.$closes = this.$lightbox.querySelectorAll('[data-lightbox="close"]');
    this.$carousel = this.$lightbox.querySelector('[data-carousel="lightbox"]');
    this.$carouselItems = null;
    this.carousel = null;
    // this.$title = this.$lightbox.querySelector('.lightbox__title');

    // this.title = this.$toggle.getAttribute('data-lightbox__title');

    // this.hide = this.hide.bind(this);

    this.initCarousel();
    this.addEventListeners();
  }

  initCarousel() {
    if (this.$carousel) {
      this.carousel = new Glide(this.$carousel, {
        type: 'slider',
      });
    }
  }

  // hide(event) {
  //   event.stopPropagation();
  //   this.$lightbox.style.display = '';
  //   // this.$backdrop.removeEventListener('transitionend', this.hide, true);
  // }

  close() {
    this.$root.classList.remove('lightbox__open');
    this.$lightbox.classList.remove('show');
    // this.$lightbox.style.opacity = '0';
    this.$lightbox.style.pointerEvents = 'none';

    this.carousel.disable();
  }

  addEventListeners() {
    this.$toggle.addEventListener('click', (event) => {
      // this.$title.innerHTML = this.title;

      // this.$lightbox.style.display = 'block';

      setTimeout(() => {
        this.$root.classList.toggle('lightbox__open');
        this.$lightbox.classList.toggle('show');
        // this.$lightbox.style.opacity = '1';
        this.$lightbox.style.pointerEvents = '';

        if (this.carousel.disabled) {
          this.carousel.enable();
        } else {
          // first time
          this.carousel.mount();
        }

      }, 30);

      event.preventDefault();
    });

    this.$closes.forEach(($close) => {
      $close.addEventListener('click', (event) => {
        // this.$backdrop.addEventListener('transitionend', this.hide, true);
        this.close();
        event.preventDefault();
      });
    });

    document.addEventListener('keydown', (event) => {
      if (event.key === 'Escape' && this.$lightbox.classList.contains('show')) {
        this.close();
      }
    });
  }
}
