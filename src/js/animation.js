import * as d3 from './d3';

const animation = () => {
  function getRandom(min, max) {
    return Math.random() * (max - min) + min;
  }

  function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

  function getScaledNumber(scale) {
    return Math.round(numCircles * scale);
  }

  const ced = [
    'M416.27 393.26c-20.78 45.55-63 74-120.36 74-84.29 0-135.41-60.94-135.41-149.65 0-88.16 53.29-150.73 136.49-150.73 54.35 0 97 27.38 117.85 70.49',
    'M862 418.08c-23.36 29.33-59.7 46.8-107 46.8-88.73 0-138.14-63.12-138.14-149.65 0-88.16 53.34-150.73 138.71-150.73 83.67 0 134.17 58.8 136 140.3L684 306',
    'M1110.17 454.41h55.95c67.06 0 95.14-11.93 121-32.53 30-24.24 45.22-61.47 45.22-110.63 0-60.35-23.73-102-70.55-123.75-25.52-11.82-53-16.64-94.84-16.64h-58.7l1.7 214.17',
  ];

  const width = 1493;
  const height = 638;
  const radius = getRandomInt(25, 60); // Math.floor(width / 27)
  const numCircles = 440 / radius;
  const dataCircles = [];
  const offset = {
    position: 0, // width * 0.0062,
    length: 0,
  };

  const widthWindow = window.innerWidth;
  const widthRatio = width / widthWindow;
  const $animation = d3.select('[data-controller="animation"]');

  const svg = d3
    .select('[data-animation-target="svg"]')
    .append('svg')
    .attr('viewBox', [0, 0, width, height]);

  function translateAlongPath(circle, path, length) {
    d3.select(circle).style('opacity', 1);

    return (time) => {
      const p = path.getPointAtLength(time * length);
      return 'translate(' + p.x + 'px,' + p.y + 'px)';
    };
  }

  function getDuration(indexPath) {
    const length = Math.floor(paths.nodes()[indexPath].getTotalLength());

    // return Math.log(length) * Math.log2(length) * getRandomInt(20, 75);
    return Math.log(length) * Math.log2(length) * 38;
  }

  function getCirclePathLength(path, circles, indexCircle) {
    // distribute evenly
    return (
      (path.getTotalLength() / circles.size()) *
      ((circles.size() - indexCircle) *
        getRandom(1 - offset.length, 1 + offset.length))
    );
  }

  function generateCircles(num = 0) {
    const circles = [];
    const totalCircles =
      getRandomInt(
        numCircles - getScaledNumber(0.125),
        numCircles + getScaledNumber(0.125)
      ) + num;
    let nCircle = 0;

    while (nCircle < totalCircles) {
      const circle = {
        r: radius,
        x: getRandomInt(offset.position * -1, offset.position * 1),
        y: getRandomInt(offset.position * -1, offset.position * 1),
      };
      circles.push(circle);

      nCircle += 1;
    }

    return circles;
  }

  // Paths
  const paths = svg
    .selectAll('path')
    .data(ced)
    .enter()
    .append('path')
    .attr('d', (d) => d)
    .attr('class', (d, i) => `path-${i}`);

  let animationTotalDuration = 0;

  // Circles
  paths.nodes().forEach((path, indexPath) => {
    dataCircles.push(generateCircles(indexPath === 1 ? 1 : 0));

    const duration = getDuration(indexPath);
    animationTotalDuration =
      duration > animationTotalDuration ? duration : animationTotalDuration;

    const circles = svg
      .selectAll(`.circle-p${indexPath}`)
      .data(dataCircles[indexPath])
      .enter()
      .append('circle')
      .attr('transform', null)
      .attr('r', (d) => d.r)
      .attr('cx', (d) => d.x)
      .attr('cy', (d) => d.y)
      .attr('class', `circle-p${indexPath}`)
      .style('opacity', 0)
      .transition()
      .delay((d, i) => i * 175)
      .duration(duration)
      .ease(d3.easeCircleOut)
      .styleTween('transform', (d, indexCircle, nodeList) =>
        translateAlongPath(
          nodeList[indexCircle],
          path,
          getCirclePathLength(path, circles, indexCircle)
        )
      );
  });

  // waith for entry animation to finish

  d3.timeout(() => {
    let simulation;

    function getCircles() {
      const circles = svg.selectAll('circle');
      const $circles = circles.nodes();

      return dataCircles.flat().map((circle, indexCircle) => {
        const style = window.getComputedStyle($circles[indexCircle]);

        // matrix(1, 0, 0, 1, 1116, 418)
        const transform = style
          .getPropertyValue('transform')
          .replace(/(matrix)|\(|\)|\s/g, '')
          .split(',');

        return {
          ...circle,
          x: circle.x + parseFloat(transform[4]),
          y: circle.y + parseFloat(transform[5]),
        };
      });
    }
    const cedCircles = getCircles();

    function forceMode() {
      $animation.on('mousemove', null);

      const circlesWithPointerData = getCircles();
      circlesWithPointerData.unshift({
        x: width / 2,
        y: height / 2,
        r: radius,
      });
      const pointer = circlesWithPointerData[0];

      const circles = svg
        .selectAll('.circle--new')
        .data(circlesWithPointerData.slice(1))
        .join('circle')
        .attr('class', 'circle--new')
        .attr('r', (d) => d.r)
        // .attr('cx', (d) => d.x)
        // .attr('cy', (d) => d.y);
        .attr('cx', 0)
        .attr('cy', 0)
        .style('transform', (d) => `translate3d(${d.x}px, ${d.y}px, 0)`);

      svg.selectAll('[class^="circle-p"]').remove();

      const ticked = () => {
        circles.style('transform', (d) => `translate3d(${d.x}px, ${d.y}px, 0)`);
      };

      simulation = d3
        .forceSimulation(circlesWithPointerData)
        .velocityDecay(0.1)
        .force('manyBody', d3.forceManyBody().strength(100))
        .force(
          'collide',
          d3
            .forceCollide()
            .radius(radius * 1)
            .iterations(5)
        )
        // .alpha(0.1)
        // .alphaDecay(.5)
        .on('tick', ticked);

      $animation.on('mousemove', (event) => {
        const p1 = d3.pointer(event);
        pointer.fx = p1[0] * widthRatio;
        pointer.fy = p1[1];
        simulation.alphaTarget(0.1).restart(); //reheat the simulation
      });
    }

    function cedMode() {
      $animation.on('mousemove', null);

      simulation.stop();

      function returnToCed($current, indexCircle) {
        const style = window.getComputedStyle($current);
        const transformNew = cedCircles[indexCircle];

        const interpolator = d3.interpolateTransformCss(
          style.getPropertyValue('transform'),
          `translate(${transformNew.x}px, ${transformNew.y}px)`
        );

        return (time) => interpolator(time);
      }

      const circless = d3.selectAll('circle');
      circless.each(function (d, indexCircle) {
        d3.select(this)
          .transition()
          .duration(500)
          .ease(d3.easeCircleOut)
          .styleTween('transform', (d, i, nodeList) =>
            returnToCed(nodeList[i], indexCircle)
          );
      });
    }

    $animation.on('mouseenter mousemove', forceMode);
    $animation.on('mouseleave', cedMode);
  }, animationTotalDuration + 1000);

  const $link = document.querySelector('[data-animation-target="link"');
  $link.addEventListener('mouseenter', () => {
    $link.parentElement.style.mixBlendMode = 'normal';
  });
  $link.addEventListener('mouseleave', () => {
    $link.parentElement.style.mixBlendMode = '';
  });
};

// export default animation;
window.addEventListener('load', animation, false);
